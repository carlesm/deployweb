from django.db import models

# Create your models here.

class Task(models.Model):
    task = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    priority = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


