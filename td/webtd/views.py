from django.shortcuts import render


from .models import Task


def home(r):
    tasks = Task.objects.order_by('-priority')
    dictionary = {'tasks': tasks}
    return render(r, 'webtd/index.html', dictionary)