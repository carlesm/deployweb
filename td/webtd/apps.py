from django.apps import AppConfig


class WebtdConfig(AppConfig):
    name = 'webtd'
